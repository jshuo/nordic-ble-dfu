/**
 * An example project that downloads a zip file, connects to a device and then flashes
 * it.
 */

import React, { Component } from 'react';
import {
  AppRegistry,
  TouchableHighlight,
  NativeModules,
  NativeEventEmitter,
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
} from 'react-native';
import { NordicDFU, DFUEmitter } from 'react-native-nordic-dfu';
import RNFetchBlob from 'rn-fetch-blob';
import BleManager from 'react-native-ble-manager';

const BleManagerModule = NativeModules.BleManager;
const bleManagerEmitter = new NativeEventEmitter(BleManagerModule);
const DEVICE_ID = 'EB:41:B9:52:E9:93';
// const DEVICE_ID = 'EC:B1:94:D8:AF:9B';

const FB = RNFetchBlob.config({
  fileCache: true,
  appendExt: 'zip',
});

export default class App extends Component {
  constructor(props) {
    super(props);
    this.handleDeviceDiscovered = this.handleDeviceDiscovered.bind(this);
    this.startScan = this.startScan.bind(this);
    this.handleStopScan = this.handleStopScan.bind(this);

    // const fs = RNFetchBlob.fs;

    // const dirs = RNFetchBlob.fs.dirs;
    // console.log("directory path: ", dirs.DownloadDir);

    // this.imagefile = dirs.DownloadDir + '/secux_2_12_11_D.zip';

    this.state = {
      imagefile: false,
      scanning: false,
      deviceFound: false,
      dfuState: 'Not started',
      progress: 0,
    };
  }

  componentDidMount() {
    DFUEmitter.addListener('DFUProgress', ({ percent }) => {
      console.log('DFU progress:', percent);
      this.setState({ progress: percent });
    });
    DFUEmitter.addListener('DFUStateChanged', ({ state }) => {
      console.log('DFU state:', state);
      this.setState({ dfuState: state });
    });

    // BleManager.enableBluetooth()
    //   .then(() => {
    //     // Success code
    //     console.log('The bluetooth is already enabled or the user confirm');
    //   })
    //   .catch((error) => {
    //     // Failure code
    //     console.log('The user refuse to enable bluetooth');
    //   });

    FB.fetch('GET', 'http://59.120.189.105/fwdnld/secux_2_12_11_D.zip').then((res) => {
      console.log('file saved to', res.path());
      this.setState({ imagefile: res.path() });
    });

    BleManager.start({ showAlert: false, allowDuplicates: false });
    bleManagerEmitter.addListener('BleManagerStopScan', this.handleStopScan);
    bleManagerEmitter.addListener(
      'BleManagerDiscoverPeripheral',
      this.handleDeviceDiscovered,
    );
    this.startScan();
  }
  saveFile() {
    const fs = RNFetchBlob.fs;

    const dirs = RNFetchBlob.fs.dirs;
    console.log("directory path: ", dirs.DownloadDir);

    const NEW_FILE_PATH = dirs.DownloadDir + '/app.zip';

    console.log("file path: ", NEW_FILE_PATH);

    fs.createFile(NEW_FILE_PATH, 'foo', 'utf8');

  }
  // #### DFU #######################################################

  startDFU() {
    console.log('Starting DFU');
    NordicDFU.startDFU({
      deviceAddress: DEVICE_ID,
      deviceName: 'SecuXBL',
      filePath: this.state.imagefile,
    })
      .then((res) => console.log('Transfer done: ', res))
      .catch(console.log);
  }

  // #### BLUETOOTH #################################################

  handleDeviceDiscovered({ id }) {
    if (id == DEVICE_ID) {
      this.setState({
        deviceFound: true,
        scanning: false,
      });
    }
  }

  handleStopScan() {
    console.log('Scan is stopped');
    if (this.state.scanning) {
      this.startScan();
    }
  }

  startScan() {
    BleManager.scan([], 3, true).then((results) => {
      console.log('Scanning...');
      this.setState({ scanning: true });
    });
  }

  // #### RENDER #########################################################

  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.welcome}>{this.state.dfuState}</Text>
        <Text style={styles.welcome}>
          {'DFU progress: ' + this.state.progress + ' %'}
        </Text>
        <Text>
          {this.state.scanning ? 'Scanning for: ' + DEVICE_ID : 'Not scanning'}
        </Text>
        <Text>
          {this.state.deviceFound
            ? 'Found device: ' + DEVICE_ID
            : 'Device not found'}
        </Text>
        <Text>
          {"fw path:" +  this.state.imagefile }
        </Text>
        <Text />
        {this.state.deviceFound ? (
          <TouchableHighlight
            style={{ padding: 10, backgroundColor: 'grey' }}
            onPress={this.startDFU.bind(this)}>
            <Text style={{ color: 'white' }}>Start DFU</Text>
          </TouchableHighlight>
        ) : null}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});
